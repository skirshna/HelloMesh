package io.left.hellomesh;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.Serializable;

import static android.content.ContentValues.TAG;

/**
 * Created by sri on 25/09/17.
 */

public class SurveyResults extends Activity {

    DatabaseAdapter db;
    String TAG = this.getClass().getName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results_layout);

        db = new DatabaseAdapter(this);

        Bundle bundle = getIntent().getExtras();
        String survey_id_String = bundle.getString("survey_Id");
        int currentSurveyID = Integer.parseInt(survey_id_String);
        Log.d (TAG, "fn SurveyResults: onCreate: SurveyID -> " + currentSurveyID );

    }

    @Override
    public void onBackPressed()
    {
        final Context context = this;
        Intent newIntent = new Intent(context, MainActivity.class);
        context.startActivity(newIntent);
        finish();

    }

}