package io.left.hellomesh;

import android.os.RecoverySystem;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
        * Created by sri on 27/09/17.
        */

public class Survey {

    int survey_id;
    String mesh_id;
    String question;
    String option1;
    String option2;
    String option3;
    String option4;
    int set;

    Survey (int survey_id, String mesh_id,
            String question,
            String option1, String option2,
            String option3, String option4,
            int set)
    {
        this.survey_id = survey_id;
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.set = set;
    }
}


class Results {
    int result_id;
    int survey_id;
    int option1;
    int option2;
    int option3;
    int option4;

    Results (int result_id, int survey_id,
             int option1, int option2,
             int option3, int option4)
    {
        this.result_id = result_id;
        this.survey_id = survey_id;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;

    }

}
