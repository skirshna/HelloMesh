package io.left.hellomesh;

/**
 * Created by sri on 27/09/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by sri on 25/09/17.
 */

public class DatabaseAdapter extends SQLiteOpenHelper {

    //utility variables
    private static final int SET = 1;
    private static final int INCREMENT = 1;
    private final static String TAG = DatabaseAdapter.class.getSimpleName();
    private Context context;

    //database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "SURVEYAPP";

    //tables
    private static final String TB_RESULTS = "RESULTS";
    private static final String TB_SURVEY = "SURVEY";
    private static final String TB_USER = "USER";

    //user table
    private static final String user_Id = "user_id";
    private static final String password = "password";
    private static final String firstname = "firstname";
    private static final String lastname = "lastname";

    // survey and results table
    private static final String questions = "questions";
    private static final String option1 = "option1";
    private static final String option2 = "option2";
    private static final String option3 = "option3";
    private static final String option4 = "option4";
    private static final String answered = "answered";
    private static final String survey_Id = "survey_id";
    private static final String results_Id = "results_id";
    private static final String mesh_Id = "mesh_id";




    //queries
    private static final String createUser =
            "CREATE TABLE " +  TB_USER
                    + " ( "
                    + user_Id +" TEXT(20) PRIMARY KEY, "
                    + password + " TEXT, "
                    + firstname + " TEXT, "
                    + lastname + " TEXT "
                    + ")";

    private static final String createSurvey =
            "CREATE TABLE " +  TB_SURVEY
                    + " ( "
                    + survey_Id +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + mesh_Id + " TEXT, "
                    + questions + " TEXT, "
                    + option1 + "  TEXT, "
                    + option2 + " TEXT, "
                    + option3 + " TEXT, "
                    + option4 + " TEXT, "
                    + answered + " INTEGER "
                    //+ user_Id + "VARCHAR(20),"
                    //+ "FOREIGN KEY ("+user_Id+") REFERENCES "+TB_USER +"("+user_Id+")"
                    +")";

    private static final String createResults =
            "CREATE TABLE " +  TB_RESULTS
                    + " ( "
                    + results_Id +" INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + survey_Id + " INTEGER NOT NULL, "
                    + option1 + " INTEGER DEFAULT 0, "
                    + option2 + " INTEGER DEFAULT 0, "
                    + option3 + " INTEGER DEFAULT 0, "
                    + option4 + " INTEGER DEFAULT 0, "
                    + " FOREIGN KEY ("+survey_Id+") REFERENCES "+TB_SURVEY +"("+survey_Id+")"
                    +" )";

    public DatabaseAdapter (Context context)
    {
        super (context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

    }

    public int getSET()
    {
        return this.SET;
    }

    @Override
    public void onCreate (SQLiteDatabase db){

        // Log.d (TAG, "Creating user table...");
        // Log.d (TAG, createUser);
        // db.execSQL(createUser);

        //create survey table
        Log.d(TAG, "Creating survey table");
        Log.d (TAG, createSurvey);
        db.execSQL(createSurvey);

        //create results table
        Log.d(TAG, "Creating results table");
        Log.d (TAG, createResults);
        db.execSQL(createResults);
    }

    @Override
    public void onUpgrade (SQLiteDatabase db ,int oldversion, int newversion){

        Log.d(TAG, "Changing database to version " + DATABASE_VERSION + ". Dropping all tables.");

        db.execSQL("DROP TABLE IF EXISTS '"+TB_RESULTS+"';");
        db.execSQL("DROP TABLE IF EXISTS '"+TB_SURVEY+"';");
        //db.execSQL("DROP TABLE IF EXISTS '"+TB_USER+"';");
        onCreate(db);
    }

    //TODO create async for user
    //TODO create async for results
    //TODO create async for survey

    protected void insertUser  (String user_Id, String password, String firstname, String lastname)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.user_Id, user_Id );
        contentValues.put(this.password, password);
        contentValues.put(this.firstname, firstname );
        contentValues.put(this.lastname, lastname);
        db.insert(TB_USER, null, contentValues);
    }

    protected boolean insertSurvey  (String mesh_Id,String questions, String option1,
                                     String option2, String option3, String option4)
    {
        Log.d (TAG, "Inserting into the DB -> " +
                questions + " " + option1 + " " + option2 + " " + option3 + " " + option4 + " " +this.SET);
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(TAG, "--------> "+ createSurvey);
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.mesh_Id, mesh_Id);
        contentValues.put(this.questions, questions);
        contentValues.put(this.option1, option1 );
        contentValues.put(this.option2, option2);
        contentValues.put(this.option3, option3);
        contentValues.put(this.option4, option4);
        contentValues.put(this.answered, this.SET);
        long results = db.insert(TB_SURVEY, null, contentValues);
        if(results == -1)
        {
            return false;
        }
        else
            return true;
    }


    protected String getColumnNameForRadioSelected (int selectedOption)
    {
        if (selectedOption == 0)
            return this.option1;
        else if (selectedOption == 1)
            return this.option2;
        else if (selectedOption == 2)
            return this.option3;
        else if (selectedOption == 4)
            return this.option4;
        else
            return null;
    }

    //function to update the radioOptionSelected into the TB_RESULTS
    //if
    protected boolean updateResult  (int arg_survey_id, int selectedOptionIndex)
    {

        Log.d(TAG, "--------> "+ createResults);
        Log.d (TAG, "updateResult: SurveyID -> " + arg_survey_id);
        Log.d (TAG, "updateResult: selectedRadioIndex -> " + selectedOptionIndex );

        SQLiteDatabase db_write;
        SQLiteDatabase db_read = this.getReadableDatabase();
        Cursor read_cursor = null;
        //fetch the survey record if already been answered
        String SelectQuery = "Select * from " + TB_RESULTS + " where " + this.survey_Id +
                                " = " + arg_survey_id +";";
        read_cursor = db_read.rawQuery(SelectQuery, null);

        Log.d (TAG, "SELECT QUERY: "+ SelectQuery +
                " read cursor contents -> " + read_cursor.getCount());

        //if the survey_id and its records are not present in the TB_ RESULTS,
        //then insert it to the TB_RESULTS. Works if you are answering the survey for the 1st time.
        if(read_cursor.getCount() <= 0)
        {
            db_write = this.getWritableDatabase();

            String selectedOptionColumnName = getColumnNameForRadioSelected (selectedOptionIndex);
            ContentValues contentValues = new ContentValues();
            contentValues.put(this.survey_Id, arg_survey_id);
            contentValues.put(selectedOptionColumnName,this.INCREMENT);
            long results = db_write.insert(TB_RESULTS, null, contentValues);
            if(results == -1)
            {
                return false;
            }
            else {
                Log.d (TAG, " Result inserted in TB_RESULTS");
                return true;
            }
        }
        else
        {
            Log.d (TAG, "survey already exist");
            return false;
        }
    }

    protected Results getResults (int arg_survey_id) {
        SQLiteDatabase db_read = this.getReadableDatabase();
        Cursor read_cursor = null;
        Results results = null;
        //fetch the survey record if already been answered
        String SelectQuery = "Select * from " + TB_RESULTS + " where " + this.survey_Id +
                " = " + arg_survey_id + ";";
        Log.d(TAG, "SELECT QUERY: " + SelectQuery);
        read_cursor = db_read.rawQuery(SelectQuery, null);

        if (read_cursor != null) {
            int count = read_cursor.getColumnCount();
            Log.d(TAG, "column count -> " + count);

            if (read_cursor.moveToFirst()) {
                do {
                    // share the option selected to the mesh peers
                     results = new Results(
                            Integer.parseInt(getColumnValueForIndex(read_cursor, this.results_Id)),
                            Integer.parseInt(getColumnValueForIndex(read_cursor, this.survey_Id)),
                            Integer.parseInt(getColumnValueForIndex(read_cursor, this.option1)),
                            Integer.parseInt(getColumnValueForIndex(read_cursor, this.option2)),
                            Integer.parseInt(getColumnValueForIndex(read_cursor, this.option3)),
                            Integer.parseInt(getColumnValueForIndex(read_cursor, this.option4))
                    );

                    Log.d(TAG, "line 231: results -> " + results.result_id + " "
                            + results.survey_id + " " + results.option1 + " "
                            + results.option2 + " " + results.option3 + " " + results.option4);
                } while (read_cursor.moveToNext());
            }
        }
        return results;
    }

    //function to get columnValue from a cursor based on columnName
    protected String getColumnValueForIndex (Cursor cursor, String columnName)
    {
        int index = cursor.getColumnIndex(columnName);
        String data = cursor.getString(index);
        return data;
    }


    //function to return the Survey TB record if question is given
    //returns survey content if the question is present. Returns Null if the record is not present
    protected Survey getSurvey (String questions)
    {
        Survey survey = null;
        Cursor cursor = null;
        String query = null;
        SQLiteDatabase db = null;

        try {
            Log.d(TAG, "entering getSurvey fn");

            db = this.getReadableDatabase();
            query = "Select * from " + TB_SURVEY + " where " + this.questions + " = " +"'" + questions +"'" +";";
            Log.d (TAG, "SELECT QUERY: "+ query);
            cursor = db.rawQuery(query, null);

            if (cursor != null) {
                int count = cursor.getColumnCount();
                Log.d(TAG, "column count -> " + count);

                if(cursor.moveToFirst())
                {
                    do {
                        String surveyId = getColumnValueForIndex(cursor, this.survey_Id);
                        int survey_id = Integer.parseInt(surveyId);
                        String myMeshID = getColumnValueForIndex(cursor, this.mesh_Id);
                        String question = getColumnValueForIndex(cursor, this.questions);
                        String opt1 = getColumnValueForIndex(cursor, this.option1);
                        String opt2 = getColumnValueForIndex(cursor, this.option2);
                        String opt3 = getColumnValueForIndex(cursor, this.option3);
                        String opt4 = getColumnValueForIndex(cursor, this.option4);
                        String answered = getColumnValueForIndex(cursor, this.answered);
                        int set = Integer.parseInt(answered);

                        Log.d (TAG, " In fn getSurvey : Fetched item from the DB -> " + survey_id +
                                " " + myMeshID +
                                " " + question +
                                " " + opt1 + " " + opt2 + " " + opt3 + " " + opt4 + " " + set );
                        survey  = new Survey(survey_id, myMeshID, question, opt1, opt2, opt3, opt4, set);

                    }while (cursor.moveToNext());

                }
            } else {
                Log.d(TAG, "Cursor is NULL");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return survey;
    }

} //class ends - DatabaseAdapter
