package io.left.hellomesh;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashSet;

import io.left.rightmesh.android.AndroidMeshManager;
import io.left.rightmesh.android.MeshService;
import io.left.rightmesh.id.MeshID;
import io.left.rightmesh.mesh.MeshManager;
import io.left.rightmesh.mesh.MeshStateListener;
import io.left.rightmesh.util.MeshUtility;
import io.left.rightmesh.util.RightMeshException;
import io.reactivex.functions.Consumer;

import static io.left.rightmesh.mesh.MeshManager.DATA_RECEIVED;
import static io.left.rightmesh.mesh.MeshManager.PEER_CHANGED;
import static io.left.rightmesh.mesh.MeshManager.REMOVED;

public class MainActivity extends Activity implements MeshStateListener {


    // Port to bind app to.
    private static final int HELLO_PORT = 9876;
    String TAG = this.getClass().getName();
    private static String mesh_id;
    String status;
    // MeshManager instance - interface to the mesh network.
    AndroidMeshManager mm = null;

    // Set to keep track of peers connected to the mesh.
    HashSet<MeshID> users = new HashSet<>();
    DatabaseAdapter db;
    TextView txtStatus;
    ResultsReceiver resultsReceiver;

    /**
     * Called when app first opens, initializes {@link AndroidMeshManager} reference (which will
     * start the {@link MeshService} if it isn't already running.
     *
     * @param savedInstanceState passed from operating system
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mm = AndroidMeshManager.getInstance(MainActivity.this, MainActivity.this);
        db = new DatabaseAdapter(this);
        txtStatus=(TextView) findViewById(R.id.txtStatus);

        //once we receive the SurveyID whose result was updated here - we can send it thro'
        // SendResults() function
        IntentFilter filter = new IntentFilter("com.left.hellomesh.SEND_SURVEYID");
        this.registerReceiver(resultsReceiver, filter);

    }

    /**
     * Called when activity is on screen.
     */
    @Override
    protected void onResume() {
        try {
            super.onResume();
            setContentView(R.layout.activity_main);
            this.addListenerOnAnswerSurvey();
            mm.resume();
        } catch (MeshService.ServiceDisconnectedException e) {
            e.printStackTrace();
        }


    }


    protected void addListenerOnAnswerSurvey()
    {
        Button answerSurvey = (Button) findViewById(R.id.answerSurvey);
        final Context context = this;

        // called when the ok button is clicked. If a radio option is selected,
        //then get else, else ask the user to select an option (using toast)
        answerSurvey.setOnClickListener (new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Log.d(TAG, "ENTER LISTENER : ANSWER SURVEY : MESH ID " + mesh_id);

                try {
                    //if the question is already answered - directly move to the result page
                    //if not, move to the survey answer page

                    String questions = getString(R.string.questionText);
                    Survey survey = db.getSurvey(questions);

                    //checks if the survey record is present in the db and if it is already answered,
                    //then direct to the survey answering activity
                    if (survey != null && survey.set == db.getSET()) {

                        Bundle bundle = new Bundle();
                        bundle.putString("survey_Id", Integer.toString(survey.survey_id));
                        Intent intent = new Intent(context, SurveyResults.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    //if already answered, direct them to survey results page
                    else
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("mesh_Id", mesh_id);
                        Intent intent = new Intent(context, SurveyActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }

                } catch (Exception e) {
                    Log.d(TAG, "exception occured");
                    e.printStackTrace();
                }
            } //end - onClick
        }); // end -  setOnClickListener

    }

    /**
     * Called when the app is being closed (not just navigated away from). Shuts down
     * the {@link AndroidMeshManager} instance.
     */
    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();
            mm.stop();
        } catch (MeshService.ServiceDisconnectedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Called by the {@link MeshService} when the mesh state changes. Initializes mesh connection
     * on first call.
     *
     * @param uuid our own user id on first detecting
     * @param state state which indicates SUCCESS or an error code
     */
    @Override
    public void meshStateChanged(MeshID uuid, int state) {
        if (state == MeshStateListener.SUCCESS) {

            try {
                // Binds this app to MESH_PORT.
                // This app will now receive all events generated on that port.
                mm.bind(HELLO_PORT);

                //survey app codes
                mesh_id = mm.getUuid().toString();
                Log.d (TAG, "meshStateChanged called init; meshID " + mesh_id);
                this.addListenerOnAnswerSurvey();

                // Subscribes handlers to receive events from the mesh.
                mm.on(DATA_RECEIVED, new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {
                        handleDataReceived((MeshManager.RightMeshEvent) o);
                    }
                });
                mm.on(PEER_CHANGED, new Consumer() {
                    @Override
                    public void accept(Object o) throws Exception {
                        handlePeerChanged((MeshManager.RightMeshEvent) o);
                    }
                });

                // If you are using Java 8 or a lambda backport like RetroLambda, you can use
                // a more concise syntax, like the following:
                // mm.on(PEER_CHANGED, this::handlePeerChanged);
                // mm.on(DATA_RECEIVED, this::dataReceived);

                // Enable buttons now that mesh is connected.
                Button btnConfigure = (Button) findViewById(R.id.btnConfigure);
                Button btnSend = (Button) findViewById(R.id.btnHello);
                btnConfigure.setEnabled(true);
                btnSend.setEnabled(true);
            } catch (RightMeshException e) {
                String status = "Error initializing the library" + e.toString();
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
                TextView txtStatus = (TextView) findViewById(R.id.txtStatus);
                txtStatus.setText(status);
                return;
            }
        }

        // Update display on successful calls (i.e. not FAILURE or DISABLED).
        if (state == MeshStateListener.SUCCESS || state == MeshStateListener.RESUME) {
            updateStatus();


        }
    }

    /**
     * Update the {@link TextView} with a list of all peers.
     */
    private void updateStatus() {
        String status = "uuid: " + mm.getUuid().toString() + "\n\n\npeers:\n";
        for (MeshID user : users) {
            status += user.toString() + "\n";
        }
        txtStatus.setText(status);

        /**
         status = "uuid: " + (mm.getUuid()==null?"":mm.getUuid().toString()) + "\npeers:\n";
        for (MeshID user : users) {
            status += user.toString() + "\n";
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtStatus.setText(status);
            }
        }); **/
    }

    /**
     * Handles incoming data events from the mesh - toasts the contents of the data.
     *
     * @param e event object from mesh
     */
    private void handleDataReceived(MeshManager.RightMeshEvent e) {
        final MeshManager.DataReceivedEvent event = (MeshManager.DataReceivedEvent) e;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Toast data contents.
                String message = new String(event.data);
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

                // Play a notification.
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(MainActivity.this, notification);
                r.play();
            }
        });
    }

    /**
     * Handles peer update events from the mesh - maintains a list of peers and updates the display.
     *
     * @param e event object from mesh
     */
    private void handlePeerChanged(MeshManager.RightMeshEvent e) {
        // Update peer list.
        MeshManager.PeerChangedEvent event = (MeshManager.PeerChangedEvent) e;
        if (event.state != REMOVED && !users.contains(event.peerUuid)) {
            users.add(event.peerUuid);
        } else if (event.state == REMOVED){
            users.remove(event.peerUuid);
        }

        // Update display.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateStatus();
            }
        });
    }

    /**
     * Sends "hello" to all known peers.
     *
     * @param v calling view
     */
    public void sendHello(View v) throws RightMeshException {
        for(MeshID receiver : users) {
            String msg = "Hello to: " + receiver + " from" + mm.getUuid();
            MeshUtility.Log(this.getClass().getCanonicalName(), "MSG: " + msg);
            byte[] testData = msg.getBytes();
            mm.sendDataReliable(receiver, HELLO_PORT, testData);
        }
    }


        /**
         * Sends "hello" to all known peers.
         *
         */
    public void sendResults( Results results) throws RightMeshException {
        for(MeshID receiver : users) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = null;

            try {
                out = new ObjectOutputStream(bos);
                out.writeObject(results);
                out.flush();
                byte[] testData  = bos.toByteArray();
                mm.sendDataReliable(receiver, HELLO_PORT, testData);
            }

            catch (IOException e) {
                Log.d(TAG, " fn: convertResultsToBytes; IO EXCEPTION");
                e.printStackTrace();
            }

            finally {
                try {
                    bos.close();
                } catch (IOException ex) {
                    // ignore close exception
                }
            }

        }
    }

    /**
     * Open mesh settings screen.
     *
     * @param v calling view
     */
    public void configure(View v)
    {
        try {
            mm.showSettingsActivity();
        } catch(RightMeshException ex) {
            MeshUtility.Log(this.getClass().getCanonicalName(), "Service not connected");
        }
    }

    public static String getMyMeshID()
    {
        return MainActivity.mesh_id;
    }



} // end of Main Activity

