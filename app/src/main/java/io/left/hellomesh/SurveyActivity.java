package io.left.hellomesh;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import static java.lang.Thread.sleep;

/**
 * Created by sri on 27/09/17.
 */

public class SurveyActivity extends Activity {


    //SurveyApp Variables
    RadioGroup radiogrp_options;
    RadioButton radio_OptionSelected;
    Button okbutton;
    int OptionSelected = -1;
    String TAG = this.getClass().getName();
    String radioButtonText;
    DatabaseAdapter db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_layout);

        /******** survey app ****/
        // add the survey to the DB
        db = new DatabaseAdapter(this);

        Bundle bundle = getIntent().getExtras();
        String myMeshID = bundle.getString("mesh_Id");
        Log.d (TAG, "My mesh ID -> " + myMeshID );


        this.InsertSurvey();
        //add the option submitted to the DB
        this.addListenerOnButton();

    }

    /**
     * Called when activity is on screen.updateStatus
     */
    @Override
    protected void onResume() {
        try {
            super.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.addListenerOnButton();
    }

    //survey Selection Activity
    //fn to fetch the question string
    protected String getQuestionText()
    {
        TextView textview_questions = (TextView) findViewById(R.id.question_text);
        String questions = textview_questions.getText().toString();
        Log.d (TAG, "fn: getQuestionText: The question fetched -> " + questions);
        return questions;
    }

    //fn to fetch the selected radio option
    protected void addListenerOnButton()
    {

        radiogrp_options = (RadioGroup) findViewById(R.id.radio_options);
        okbutton = (Button) findViewById(R.id.okbutton);
        final Context context = this;

        // called when the ok button is clicked. If a radio option is selected,
        //then get else, else ask the user to select an option (using toast)
        okbutton.setOnClickListener (new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                try {
                    OptionSelected = radiogrp_options.getCheckedRadioButtonId();
                    radio_OptionSelected = (RadioButton) findViewById(OptionSelected);
                    radioButtonText = radio_OptionSelected.getText().toString();
                    int selectedRadioIndex = radiogrp_options.indexOfChild(radio_OptionSelected);

                    Log.d (TAG, "textview_questions value -> " + getQuestionText());
                    Log.d(TAG, "Value of OptionSelected -> " + OptionSelected);
                    Log.d(TAG, "Selected Option text  -> " + radioButtonText);

                    //update the TB RESULTS based on the options selected
                    String questions = getQuestionText();
                    Survey survey = db.getSurvey(questions);
                    boolean returnvalue = db.updateResult(survey.survey_id, selectedRadioIndex);
                    Log.d (TAG, "fn addListenerOnButton: returnvalue : " + returnvalue);

                    if(returnvalue == true)
                    {
                        //update the mesh network if the selected value is inserted into the app
                        Results results = db.getResults(survey.survey_id);
                        if (results != null)
                            Log.d(TAG, "fn SurveyResults: onCreate -> " + results.result_id + " " + results.survey_id
                                    + " " + results.option1 + " " + results.option2 + " " + results.option3
                                    + " " + results.option4);
                        else
                            Log.d (TAG, "fn SurveyResults:OnCreate -> Result was null");

                        //TODO - somehow call the sendResuls function in the mesh
                        broadcastIntent(survey.survey_id);
                    }

                    Intent intent = new Intent(context, SurveyResults.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("survey_Id", Integer.toString(survey.survey_id));
                    intent.putExtras(bundle);
                    startActivity(intent);

                }//end - try

                catch (NullPointerException e)
                {
                    Log.d(TAG, "Value of OptionSelected -> " + OptionSelected);
                    Toast.makeText(getApplication(), "Please select an option", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Radio_OptionSelected is NULL");
                }//end-catch


            } //end - onClick
        }); // end -  setOnClickListener
    } //end - addListenerOnButton()


    //fn to enter the survey details to the database
    protected void InsertSurvey ()
    {
        final Context context = this;
        String mesh_id = MainActivity.getMyMeshID();

        //textview_questions = (TextView) findViewById(R.id.question_text);
        radiogrp_options = (RadioGroup) findViewById(R.id.radio_options);

        //String questions = textview_questions.getText().toString();
        String questions = getQuestionText();
        String option1 =  ((RadioButton) findViewById(R.id.radio1)).getText().toString();
        String option2 =  ((RadioButton) findViewById(R.id.radio2)).getText().toString();

        Log.d (TAG, "Insert Survey fn: " + "textview_questions value -> " + questions );
        Log.d(TAG, "InsertSurvey fn: "+ "Selected Option text  -> " + option1);
        Log.d(TAG, "InsertSurvey fn: "+ "Selected Option text  -> " + option2);

        boolean returnvalue = db.insertSurvey(mesh_id, questions, option1, option2, null, null);

        Log.d (TAG,"Return Value -> " + returnvalue);
        if (returnvalue == true)
        {
            Log.d(TAG, "Insertion was successful, returnvalue -> " + returnvalue);
        }
        else
        {
            Log.d(TAG, "Insertaion failed! returnvalue -> " + returnvalue);
        }


    }

    public void broadcastIntent ( int arg_survey_id)
    {
        Log.d (TAG, "In fn broadcastIntent : Sent Survey ID ->" + arg_survey_id);
        Bundle bundle = new Bundle();
        bundle.putString("SurveyUpdated", Integer.toString(arg_survey_id));
        Intent intent = new Intent();
        intent.setAction("com.left.hellomesh.SEND_SURVEYID");
        intent.putExtras(bundle);
        sendBroadcast(intent);
    }


}
